CREATE DATABASE sambler_db
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Spanish_Colombia.1252'
       LC_CTYPE = 'Spanish_Colombia.1252'
       CONNECTION LIMIT = -1;



INSERT INTO roles(name) VALUES('ROLE_USER'); 
INSERT INTO roles(name) VALUES('ROLE_ADMIN');