package com.sambler.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sambler.demo.request.TagRequest;
import com.sambler.demo.response.PagedResponse;
import com.sambler.demo.response.TagResponse;
import com.sambler.demo.service.TagService;
import com.sambler.demo.util.AppConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;



@RestController
@Api(tags = "Tag Endpoint",value = "Tag Management System", description = "Operations pertaining to tag in Sambler Management System")
@RequestMapping(path = "/tags",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class TagController {
	@Autowired
	TagService tagService;
	
	@ApiOperation(value = "Find all Tags",notes = "Return all Tags")
	@GetMapping
	public ResponseEntity<PagedResponse<TagResponse>> getEditorials(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		return ResponseEntity.ok(tagService.getTags(page, size));
		
	}
	
	@ApiOperation(value = "Create new tag", notes = "Creates new  tag. Returns created tag with id.",response = TagResponse.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TagResponse> createAuthor(@Valid @RequestBody TagRequest tagRequest){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(tagService.createTag(tagRequest));
	}
	
}
