package com.sambler.demo.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sambler.demo.config.WebSocketConfig;
import com.sambler.demo.request.PublicationRequest;
import com.sambler.demo.response.PagedResponse;
import com.sambler.demo.response.PublicationResponse;
import com.sambler.demo.security.CurrentUser;
import com.sambler.demo.security.UserPrincipal;
import com.sambler.demo.service.PublicationActionService;
import com.sambler.demo.service.PublicationService;
import com.sambler.demo.util.AppConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "Publication Endpoint",value = "Publication Management System", description = "Operations pertaining to Publication in Sambler Management System")
@RequestMapping(path = "/api/publications",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class PublicationController {
	
	@Autowired
	PublicationService publicationService;
	
	@Autowired
	PublicationActionService publicationActionService;
	
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@ApiOperation(value = "Find all Tags",notes = "Return all Tags")
	@GetMapping
	public ResponseEntity<PagedResponse<PublicationResponse>> getPublications(
			@CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		return ResponseEntity.ok(publicationService.getPublications(page, size,currentUser));
		
	}
	
	
	
	@ApiOperation(value = "Create new Publication", notes = "Creates new  Publication. Returns created Publication with id.",response = PublicationResponse.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PublicationResponse> createPublication(@Valid @RequestBody PublicationRequest publicationRequest){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(publicationService.createPublication(publicationRequest));
	}
	
	@ApiOperation(value = "Get Publication by id", notes = "Returns Publication for id specified.",response = PublicationResponse.class)
	@GetMapping("/{publicationId}")
	public ResponseEntity<PublicationResponse> getAuthors(
			@CurrentUser UserPrincipal currentUser,
			@PathVariable(value = "publicationId") Long publicationId) {
		
		return ResponseEntity.ok(publicationService.getPublicationById(publicationId,currentUser));
	}
	
	@ApiOperation(value = "Like Publication", notes = "Add or remove like in publication. Returns Publication",response = PublicationResponse.class)
	@PostMapping(path = "/{publicationId}/like")
	public ResponseEntity<PublicationResponse> LikePublication(@CurrentUser UserPrincipal currentUser,
			@ApiParam(value = "id of the publication", required = true) @PathVariable(value = "publicationId") Long publicationId){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(publicationActionService.LikePublication(publicationId, currentUser));
	}
	
	@ApiOperation(value = "Share Publication", notes = "Add or remove Share in publication. Returns Publication",response = PublicationResponse.class)
	@PostMapping(path = "/{publicationId}/share")
	public ResponseEntity<PublicationResponse> SharePublication(@CurrentUser UserPrincipal currentUser,
			@ApiParam(value = "id of the publication", required = true) @PathVariable(value = "publicationId") Long publicationId){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(publicationActionService.SharePublication(publicationId, currentUser));
	}
	
	@GetMapping("/prueba")
	public ResponseEntity<String> prueba(){
		
		messagingTemplate.convertAndSendToUser("juancuero25", "/queue/reply","el mensaje");
		return ResponseEntity.status(HttpStatus.CREATED).body("hola");
	}
	
	@GetMapping("/conectados")
	public ResponseEntity<String> prueba2(){
		
		Set<String> usersSocket =WebSocketConfig.getUserSocket();
		
		return ResponseEntity.status(HttpStatus.CREATED).body("hola"+usersSocket);
	}
	
}
