package com.sambler.demo.controller;

import com.sambler.demo.exception.ResourceNotFoundException;
import com.sambler.demo.entity.User;
import com.sambler.demo.repository.UserRepository;
import com.sambler.demo.request.*;
import com.sambler.demo.response.UserIdentityAvailability;
import com.sambler.demo.response.UserSummary;
import com.sambler.demo.security.UserPrincipal;
import com.sambler.demo.security.CurrentUser;
import com.sambler.demo.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;


    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName(),currentUser.getEmail());
        return userSummary;
    }

    



   

}
