package com.sambler.demo.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import com.sambler.demo.entity.Tag;
import com.sambler.demo.exception.BadRequestException;
import com.sambler.demo.repository.TagRepository;
import com.sambler.demo.request.TagRequest;
import com.sambler.demo.response.PagedResponse;
import com.sambler.demo.response.TagResponse;
import com.sambler.demo.util.AppConstants;



@Service
public class TagService {
	
	@Autowired
    TagRepository tagRepository;
	
	@Autowired
	ModelMapper modelMapper;
	
	public TagResponse createTag(TagRequest tagRequest) {
		
		Tag tag = modelMapper.map(tagRequest, Tag.class);

		tagRepository.save(tag);
		
		TagResponse tagResponse = modelMapper.map(tag, TagResponse.class);
		
		return tagResponse;
		
	}
	
	public PagedResponse<TagResponse> getTags(int page, int size) {
		validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Tag> tags = tagRepository.findAll(pageable);
        if (tags.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), tags.getNumber(),
            		tags.getSize(), tags.getTotalElements(), tags.getTotalPages(), tags.isLast());
        }

        Type listType = new TypeToken<List<TagResponse>>(){}.getType();
		List<TagResponse> tagResponses = modelMapper.map(tags.getContent(),listType);

        return new PagedResponse<>(tagResponses, tags.getNumber(),
        		tags.getSize(), tags.getTotalElements(), tags.getTotalPages(), tags.isLast());
    }
	
	public Set<Tag> getTagsInId(Set<Long> ids) {
		return tagRepository.findByIdIn(ids);
	}
	
	private void validatePageNumberAndSize(int page, int size) {
		
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
        
    }
}
