package com.sambler.demo.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import com.sambler.demo.entity.Publication;
import com.sambler.demo.entity.Tag;
import com.sambler.demo.repository.PublicationRepository;
import com.sambler.demo.request.PublicationRequest;
import com.sambler.demo.response.PagedResponse;
import com.sambler.demo.response.PublicationResponse;
import com.sambler.demo.response.TagResponse;
import com.sambler.demo.security.UserPrincipal;
import com.sambler.demo.util.AppConstants;
import com.sambler.demo.exception.BadRequestException;
import com.sambler.demo.exception.ResourceNotFoundException;


@Service
public class PublicationService {
	
	@Autowired
    PublicationRepository publicationRepository;
	
	@Autowired
	TagService tagService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	PublicationActionService publicacionActionService;
	
	@Autowired
	ModelMapper modelMapper;
	
	
	public PublicationResponse createPublication(PublicationRequest publicationRequest) {
		
		Set<Tag> tagsSet = tagService.getTagsInId(publicationRequest.getTagsIds());
		List<Tag> tags = new ArrayList<>(tagsSet);
		Publication publication = modelMapper.map(publicationRequest, Publication.class);
		publication.setTags(tags);
		publicationRepository.save(publication);
		
		PublicationResponse publicationResponse = modelMapper.map(publication, PublicationResponse.class);
		publicationResponse.setUser(userService.getUserById(publication.getCreatedBy()));
		return publicationResponse;
		
	}
	
	public PublicationResponse getPublicationById(Long publicationId,UserPrincipal currentUser) {
		Publication publication = publicationRepository.findById(publicationId).orElseThrow(
                () -> new ResourceNotFoundException("Publication", "id", publicationId));
		
		PublicationResponse publicationResponse = modelMapper.map(publication, PublicationResponse.class);
		publicationResponse.setUser(userService.getUserById(publication.getCreatedBy()));
		publicationResponse.setCantLikes(publicacionActionService.getLikesByPublicationId(publicationId));
		publicationResponse.setCantShares(publicacionActionService.getSharesByPublicationId(publicationId));
		publicationResponse.setLiked(publicacionActionService.likedPublication(publicationId, currentUser));
		publicationResponse.setShared(publicacionActionService.sharedPublication(publicationId, currentUser));
		Date createdAt = Date.from(publication.getCreatedAt());
		publicationResponse.setCreatedAt(createdAt);
		
		
		return publicationResponse;
	}
	
	public PagedResponse<PublicationResponse> getPublications(int page, int size,UserPrincipal currentUser) {
		validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Publication> publications = publicationRepository.findAll(pageable);
        if (publications.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), publications.getNumber(),
            		publications.getSize(), publications.getTotalElements(), publications.getTotalPages(), publications.isLast());
        }
        
        Type listType = new TypeToken<List<PublicationResponse>>(){}.getType();
		//List<BorrowResponse> borrowResponses = modelMapper.map(borrows.getContent(),listType);
		List<PublicationResponse> publicationsResponses = new ArrayList<PublicationResponse>();
		
		publications.getContent().forEach(p -> publicationsResponses.add(convert(p,currentUser)));
        


        return new PagedResponse<>(publicationsResponses, publications.getNumber(),
        		publications.getSize(), publications.getTotalElements(), publications.getTotalPages(), publications.isLast());
    }
	
	private void validatePageNumberAndSize(int page, int size) {
		
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
        
    }
	
	public PublicationResponse convert(Publication publication,UserPrincipal currentUser) {
		
		PublicationResponse publicationResponse = modelMapper.map(publication, PublicationResponse.class);
		publicationResponse.setUser(userService.getUserById(publication.getCreatedBy()));
		publicationResponse.setCantLikes(publicacionActionService.getLikesByPublicationId(publication.getId()));
		publicationResponse.setCantShares(publicacionActionService.getSharesByPublicationId(publication.getId()));
		publicationResponse.setLiked(publicacionActionService.likedPublication(publication.getId(), currentUser));
		publicationResponse.setShared(publicacionActionService.sharedPublication(publication.getId(), currentUser));
		Date createdAt = Date.from(publication.getCreatedAt());
		publicationResponse.setCreatedAt(createdAt);
		
		return publicationResponse;
	}
	
	
	
}
