package com.sambler.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sambler.demo.entity.PublicationAction;
import com.sambler.demo.repository.PublicationActionRepository;
import com.sambler.demo.response.PublicationResponse;
import com.sambler.demo.security.UserPrincipal;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class PublicationActionService {
	
	@Autowired
    PublicationActionRepository publicationActionRepository;
	
	@Autowired
	PublicationService publicationService;
	
	@Autowired
	UserService userService;
	
	public PublicationAction createPublicationAction(Long publicationId,  UserPrincipal currentUser) {
		PublicationAction publicationAction = new PublicationAction();
		publicationAction.setPublicationId(publicationService.getPublicationById(publicationId,currentUser).getId());
		publicationAction.setUserId(currentUser.getId());
		return publicationActionRepository.save(publicationAction);	
	}
	
	public PublicationResponse LikePublication(Long publicationId, UserPrincipal currentUser) {
		Optional<PublicationAction> publicationActionO = publicationActionRepository.findByPublicationIdAndUserId(publicationId, currentUser.getId());
		log.info("inside method LikePublication() ");    
		PublicationAction publicationAction = new PublicationAction();
		if (!publicationActionO.isPresent()) {
			publicationAction = createPublicationAction(publicationId,currentUser);
		}else{
			publicationAction = publicationActionO.get();
		}
		publicationAction.setLiked(!publicationAction.getLiked());
		publicationActionRepository.save(publicationAction);
		return publicationService.getPublicationById(publicationId, currentUser);
	}
	
	public PublicationResponse SharePublication(Long publicationId, UserPrincipal currentUser) {
		Optional<PublicationAction> publicationActionO = publicationActionRepository.findByPublicationIdAndUserId(publicationId, currentUser.getId());
		log.info("inside method LikePublication() ");    
		PublicationAction publicationAction = new PublicationAction();
		if (!publicationActionO.isPresent()) {
			publicationAction = createPublicationAction(publicationId,currentUser);
		}else{
			publicationAction = publicationActionO.get();
		}
		publicationAction.setShared(!publicationAction.getShared());
		publicationActionRepository.save(publicationAction);
		return publicationService.getPublicationById(publicationId,currentUser);
	}
	
	public Long getLikesByPublicationId(Long publicationId) {
		return publicationActionRepository.countByPublicationIdAndLiked(publicationId, true);
	}
	
	public Long getSharesByPublicationId(Long publicationId) {
		return publicationActionRepository.countByPublicationIdAndShared(publicationId, true);
	}
	
	public Boolean likedPublication(Long publicationId, UserPrincipal currentUser) {
		return publicationActionRepository.existsByPublicationIdAndLikedAndUserId(publicationId,true, currentUser.getId());
	}
	
	public Boolean sharedPublication(Long publicationId, UserPrincipal currentUser) {
		return publicationActionRepository.existsByPublicationIdAndSharedAndUserId(publicationId,true, currentUser.getId());
	}
}
