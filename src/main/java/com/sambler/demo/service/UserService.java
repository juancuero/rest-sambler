package com.sambler.demo.service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sambler.demo.entity.User;
import com.sambler.demo.exception.ResourceNotFoundException;
import com.sambler.demo.repository.UserRepository;
import com.sambler.demo.response.UserSummary;

@Service
public class UserService {
	
	@Autowired
    UserRepository userRepository;
	
	@Autowired
	ModelMapper modelMapper;
	
	public Set<UserSummary> getUserInId(Set<Long> ids) {
		Set<User> usersSet = userRepository.findByIdIn(ids);
		Type listType = new TypeToken<Set<UserSummary>>(){}.getType();
		Set<UserSummary> users = modelMapper.map(usersSet, listType);
		return users;
	}
	
	public UserSummary getUserById(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", userId));
		
		UserSummary userSummary = modelMapper.map(user, UserSummary.class);
		return userSummary;

    }

}
