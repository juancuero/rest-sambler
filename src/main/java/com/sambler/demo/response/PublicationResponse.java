package com.sambler.demo.response;

import java.util.Date;
import java.util.Set;


import lombok.Data;

@Data
public class PublicationResponse {
	
	private Long id;
	
	private String title;
	
	private String description;
	
	private UserSummary user;
	
	private Set<TagResponse> tags ;
	
	private Long cantLikes = 0L;
	
	private Long cantShares = 0L;
	
	private Boolean liked = false;
	
	private Boolean shared  = false;
	
	private Date createdAt;
	
}
