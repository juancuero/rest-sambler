package com.sambler.demo.response;

import lombok.Data;

@Data
public class TagResponse {
	
	private Long id;
	
    private String name;
}
