package com.sambler.demo.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sambler.demo.entity.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
	Set<Tag> findByIdIn(Set<Long> ids);
}
