package com.sambler.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sambler.demo.entity.Publication;

@Repository
public interface PublicationRepository extends JpaRepository<Publication, Long> {


}
