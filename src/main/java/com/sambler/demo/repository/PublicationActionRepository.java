package com.sambler.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.sambler.demo.entity.PublicationAction;



@Repository
public interface  PublicationActionRepository extends JpaRepository<PublicationAction, Long> {
	
	Optional<PublicationAction> findByPublicationIdAndUserId(Long publicationId, Long userId);
	
	Long countByPublicationIdAndLiked(Long publicationId, Boolean like);
	
	Long countByPublicationIdAndShared(Long publicationId, Boolean shared);
	
	Boolean existsByPublicationIdAndLikedAndUserId(Long publicationId,Boolean liked, Long userId);
	
	Boolean existsByPublicationIdAndSharedAndUserId(Long publicationId,Boolean shared, Long userId);
}
