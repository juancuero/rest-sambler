package com.sambler.demo.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



import lombok.Data;
import lombok.NoArgsConstructor;

import com.sambler.demo.entity.embeddable.PublicationUser;

@Data
@NoArgsConstructor
@Entity
@IdClass(PublicationUser.class)
@Table(name = "publications_actions")
public class PublicationAction implements Serializable {
	

	@Id 
	Long publicationId;
	
	@Id
	Long userId;
	
	@Column(columnDefinition = "boolean default false")
    private Boolean liked = false;
	
	@Column(columnDefinition = "boolean default false")
    private Boolean shared = false;
	
}
