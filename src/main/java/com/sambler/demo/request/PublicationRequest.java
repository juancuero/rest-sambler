package com.sambler.demo.request;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PublicationRequest {
	
	@NotBlank
    @Size(max = 40)
    private String title;
	
	@NotBlank
    @Size(max = 500)
    private String description;
	
	@ApiModelProperty(notes = "Tags ids", example = "[2,3]")
	private Set<Long> tagsIds = new HashSet<>();
}
