package com.sambler.demo.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


import lombok.Data;

@Data
public class TagRequest {
	@NotBlank
    @Size(max = 40)
    private String name;
}
